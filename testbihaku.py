#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import sys

from PIL import Image
from PIL import ImageFilter

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("USAGE: testbihaku.py infile [outfile]")
        sys.exit(0)

    source_image = Image.open(sys.argv[1])
    destination_image = source_image.copy()
    source_image = source_image.filter(ImageFilter.GaussianBlur(20))
    for i in range(source_image.size[1]):
        for j in range(source_image.size[0]):
            source_value = source_image.getpixel((j, i))
            destination_value = list(destination_image.getpixel((j, i)))
            for m in range(3):
                value = source_value[m] / 2.0
                destination_value[m] = int((1.0 - ((1.0 - value / 255.0) * (1.0 - destination_value[m] / 255.0))) * 255.0)
            destination_image.putpixel((j, i), tuple(destination_value))
        print("Processing: {0:.2f}%".format(i / destination_image.size[1] * 100.0))
    destination_image.show()
    if len(sys.argv) >= 3:
        destination_image.save(sys.argv[2])
        print("Image saved.")
